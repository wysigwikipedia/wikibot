#![feature(lazy_cell)]

use dotenvy::dotenv;
use regex::Regex;
use reqwest;
use reqwest::header::{HeaderMap, AUTHORIZATION, CONTENT_LENGTH, CONTENT_TYPE};
use reqwest::Client;
use serde::{Deserialize, Serialize};
use std::env;
use std::sync::LazyLock;

#[derive(Debug, Deserialize)]
struct Backlink {
    #[serde(rename = "pageid")]
    page_id: usize,
    ns: usize,
    title: String,
    redirect: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct BacklinkQuery {
    pub backlinks: Vec<Backlink>,
}

#[derive(Debug, Deserialize)]
pub struct Continue {
    #[serde(rename = "blcontinue")]
    pub bl_continue: String,
    pub r#continue: String,
}

#[derive(Debug, Deserialize)]
pub struct BacklinkResponse {
    #[serde(rename = "batchcomplete")]
    pub batch_complete: Option<String>,
    pub r#continue: Option<Continue>,
    pub query: BacklinkQuery,
}

#[derive(Debug, Deserialize)]
pub struct Content {
    pub title: String,
    #[serde(rename = "pageid")]
    pub page_id: usize,
    pub wikitext: String,
}

#[derive(Debug, Deserialize)]
pub struct ContentResponse {
    pub parse: Content,
}

#[derive(Debug, Deserialize)]
pub struct Tokens {
    #[serde(rename = "csrftoken")]
    pub csrf_token: String,
}

#[derive(Debug, Deserialize)]
pub struct CSRFTokenQuery {
    pub tokens: Tokens,
}

#[derive(Debug, Deserialize)]
pub struct CSRFTokenResponse {
    #[serde(rename = "batchcomplete")]
    pub batch_complete: String,
    pub query: CSRFTokenQuery,
}

#[derive(Debug, Serialize)]
pub struct Body {
    pub bot: bool,
    pub text: String,
    pub token: String,
    pub summary: String,
}

const CLIENT: LazyLock<Client> = LazyLock::new(|| {
    dotenv().unwrap();

    let mut headers = HeaderMap::new();
    headers.insert(
        AUTHORIZATION,
        format!("Bearer {}", env::var("ACCESS_TOKEN").unwrap())
            .parse()
            .unwrap(),
    );
    headers.insert(
        CONTENT_TYPE,
        "application/x-www-form-urlencoded".parse().unwrap(),
    );
    let client = reqwest::Client::builder()
        .default_headers(headers)
        .build()
        .unwrap();
    client
});

async fn get_backlinks(title: String, bl_continue: Option<String>) -> BacklinkResponse {
    let mut url = format!("https://af.wikipedia.org/w/api.php?action=query&format=json&list=backlinks&bltitle={title}");
    if let Some(bl_continue) = bl_continue {
        url = format!("{url}&blcontinue={bl_continue}");
    }
    match reqwest::get(url).await {
        Ok(resp) => {
            match resp.text().await {
                Ok(text) => {
                    return serde_json::from_str::<BacklinkResponse>(&text).unwrap();
                }
                Err(e) => {
                    panic!("{:?}", e);
                }
            };
        }
        Err(e) => {
            panic!("{:?}", e);
        }
    }
}

async fn get_content(page_id: usize) -> ContentResponse {
    let url = format!("https://af.wikipedia.org/w/api.php?action=parse&pageid={page_id}&prop=wikitext&format=json&formatversion=2");
    match reqwest::get(url).await {
        Ok(resp) => {
            match resp.text().await {
                Ok(text) => {
                    return serde_json::from_str::<ContentResponse>(&text).unwrap();
                }
                Err(e) => {
                    panic!("{:?}", e);
                }
            };
        }
        Err(e) => {
            panic!("{:?}", e);
        }
    }
}

async fn get_csrf_token() -> CSRFTokenResponse {
    let url = format!("https://af.wikipedia.org/w/api.php?action=query&format=json&meta=tokens");
    match CLIENT.get(url).send().await {
        Ok(resp) => {
            match resp.text().await {
                Ok(text) => {
                    return serde_json::from_str::<CSRFTokenResponse>(&text).unwrap();
                }
                Err(e) => {
                    panic!("{:?}", e);
                }
            };
        }
        Err(e) => {
            panic!("{:?}", e);
        }
    }
}

async fn update_content(page_id: usize, content: String) {
    let mut csrf_token = get_csrf_token().await.query.tokens.csrf_token;
    let url =
        format!("https://af.wikipedia.org/w/api.php?action=edit&pageid={page_id}&format=json");
    let body = Body {
        bot: true,
        summary: "Vervang \"Lys van Erica spesies\" met \"Lys van Erica-spesies\"".to_string(),
        text: content,
        token: csrf_token,
    };
    let body_serialized = serde_urlencoded::to_string(&body).unwrap();
    match CLIENT.post(url).body(body_serialized).send().await {
        Ok(resp) => {
            match resp.text().await {
                Ok(text) => {
                    println!("{text}");
                }
                Err(e) => {
                    panic!("{:?}", e);
                }
            };
        }
        Err(e) => {
            panic!("{:?}", e);
        }
    }
}

#[tokio::main]
async fn main() {
    let mut backlinks_response = get_backlinks("Lys van Erica spesies".to_string(), None).await;
    println!("{:?}", backlinks_response);

    let mut backlinks = backlinks_response.query.backlinks;
    let re = Regex::new(r"Lys van Erica spesies").unwrap();
    loop {
        for backlink in backlinks {
            println!("{}", backlink.title);
            let mut content = get_content(backlink.page_id).await.parse.wikitext;
            content = re
                .replace_all(content.as_str(), "Lys van Erica-spesies")
                .to_string();
            update_content(backlink.page_id, content).await;
        }
        if let Some(r#continue) = backlinks_response.r#continue {
            println!("{}", r#continue.bl_continue);
            let bl_continue = r#continue.bl_continue;
            backlinks_response =
                get_backlinks("Lys van Erica spesies".to_string(), Some(bl_continue)).await;
            backlinks = backlinks_response.query.backlinks;
        } else {
            break;
        }
    }
}
